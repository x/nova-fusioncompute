===============================
nova-fusioncompute
===============================

nova-fusioncompute is Huawei FusionCompute[1] virtualization driver for OpenStack Nova

Please fill here a long description which must be at least 3 lines wrapped on
80 cols, so that distribution package maintainers can use it in their packages.
Note that this is a hard requirement.

* Free software: Apache license
* Documentation: http://docs.openstack.org/developer/nova-fusioncompute
* Source: http://git.openstack.org/cgit/openstack/nova-fusioncompute
* Bugs: http://bugs.launchpad.net/nova-fusioncompute

Features
--------

* TODO
