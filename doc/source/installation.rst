============
Installation
============

At the command line::

    $ pip install nova-fusioncompute

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv nova-fusioncompute
    $ pip install nova-fusioncompute
